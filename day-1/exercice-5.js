export const my_array_alpha = (string) => {
    //check if the parameter is a string
    if(typeof(string) !== 'string') return 0
    //return the spread operator operation on the string to parse it as a array
    return [...string]
};
