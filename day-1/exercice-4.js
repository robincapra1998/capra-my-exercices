export const my_size_alpha = (string) => {
    //init buffer
    let buffer = 0;
    //check the type of the parameter
    if(typeof(string) !== 'string') return 0
    //iterate through the string with the buffer as index
    while(string[buffer]){
        //incremente the buffer
        buffer++;
    }
    //return the buffer
    return buffer
};
