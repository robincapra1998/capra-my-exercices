export const my_sum = (a, b) => {
    //check if we have value in parameter
    if (!a && !b){
      return 0;
    }
    //check if values are numbers
    if(typeof a === 'number' || typeof b === 'number'){
        //return a+ b
        return a + b;
    }

};