export const my_is_posi_neg = (nbr) => {
    //return positif as a string if parameter is undefined null or positif
    if (nbr >= 1 || nbr === undefined || nbr === null){
      return "POSITIF";
    } else if(nbr === 0) {
    //return neutral if the parameter is zero
      return "NEUTRAL";
    }
    //return negatif if parameter is none of condition before
    else{
        return "NEGATIF";
      }
};
  