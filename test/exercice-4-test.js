import assert from "assert";
import { my_size_alpha } from "../day-1/exercice-4.js";
import { expect } from 'chai';

describe("my_size_alpha", function () {

it("should return the string length", function () {
    var test = my_size_alpha("test");
    assert.equal(test, 4);
    });

it("should return 0 if parameter is not a string", function () {
    var test = my_size_alpha(897654);
    assert.equal(test, 0);
    });
});

