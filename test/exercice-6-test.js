import assert from "assert";
import { my_length_array } from "../day-1/exercice-6.js";
import  *  as chai from 'chai';

describe("my_length_array", function () {

it("should return an int the size of the array", function () {
    var arr = [2,1,2]
    var test = my_length_array(arr);
    chai.assert.equal(test, 3);
    });
});

