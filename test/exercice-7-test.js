import assert from "assert";
import { my_is_posi_neg } from "../day-1/exercice-7.js";
import  *  as chai from 'chai';

describe("my_is_posi_neg", function () {

    it("should return POSITIF when passing a positif number or undefined or null", function () {
        var test = my_is_posi_neg(5);
        var test1 = my_is_posi_neg(null);
        var test2 = my_is_posi_neg(undefined);
        chai.assert.equal(test, "POSITIF");
        chai.assert.equal(test1, "POSITIF");
        chai.assert.equal(test2, "POSITIF");
    });

    it("should return NEGATIF when passing a negatif number", function () {
        var test = my_is_posi_neg(-5);
        chai.assert.equal(test, "NEGATIF");
    });

    it("should return NEUTRAL when passing a neutral number", function () {
        var test = my_is_posi_neg(0);
        chai.assert.equal(test, "NEUTRAL");
    });
});

