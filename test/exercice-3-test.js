import assert from "assert";
import { my_alpha_number } from "../day-1/exercice-3.js";
import { expect } from 'chai';

describe("my_alpha_number", function () {

it("should return a string with the number value", function () {
    var test = my_alpha_number(123);
    assert.equal(test, "123");
    expect(test).to.have.string('123');
    });


it("parametre should be a string", function () {
    var test = my_alpha_number(123);
    expect(test).to.have.string('123');
    });
});

