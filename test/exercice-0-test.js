import assert from "assert";
import {my_sum } from "../day-1/exercice-0.js";

describe("my_sum", function () {

it("should work onlywith number parameter", function () {
    var test = my_sum(2,1);
    var test2 = my_sum('a','c')
    assert.equal(test, 3);
    assert.equal(test2, undefined);
});

  it("should return 3 when passing 2 and 1", function () {
    var test = my_sum(2,1);
    assert.equal(test, 3);
  });

});

