import assert from "assert";
import { my_display_alpha } from "../day-1/exercice-1.js";

describe("my_display_alpha", function () {

it("should return the alphabet", function () {
    var test = my_display_alpha();
    assert.equal(test, "abcdefghijklmnopqrstuvwxyz");
    });
});

