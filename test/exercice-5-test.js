import assert from "assert";
import { my_array_alpha } from "../day-1/exercice-5.js";
import  *  as chai from 'chai';

describe("my_array_alpha", function () {

it("should return an array with the good lenght", function () {
    var test = my_array_alpha("test");
    chai.assert.isTrue(Array.isArray(test));
    chai.assert.equal(test.length, 4);
    });
});

