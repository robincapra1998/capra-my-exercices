import assert from "assert";
import { my_display_alpha_reverse } from "../day-1/exercice-2.js";

describe("my_display_alpha_reverse", function () {

it("should return the alphabet in reverse", function () {
    var test = my_display_alpha_reverse();
    assert.equal(test, "zyxwvutsrqponmlkjihgfedcba");
    });
});

