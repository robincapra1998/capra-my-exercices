# capra-my-exercices



## Getting started

For this project you just need node.

## Build

To build when you installed node juste type 

`npm i`

It will instal chai and mocha for the testing part.

All the rest is pure vanilla javascript


## launch each exercice

You can find all exercices in the folder day-1.

They all .js file to launch them just do 

`node day-1/{exercice}.js`

## launch the testes

the tests are develop in the testes folder.

I put a script config in the package.json, you just need to right 
`npm test`

at the root of the project to launch it.

It will pass all the tests for the exercice and you will se in the terminal the result.

## CI/CD Yaml template

At the root of the project you will found a Yaml file, this proc is a ci/cd template for gitlad it will create the jobs for the test and the build
for you.

